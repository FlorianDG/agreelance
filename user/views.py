from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import Profile
from .forms import SignUpForm, EditUserForm, EditProfileForm


def index(request):
    return render(request, 'base.html')


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)

        if form.is_valid():
            user = form.save()
            user.refresh_from_db()

            user.profile.company = form.cleaned_data.get('company')

            user.is_active = False
            user.profile.categories.add(*form.cleaned_data['categories'])
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            from django.contrib import messages
            messages.success(request, 'Your account has been created and is awaiting verification.')
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'user/signup.html', {'form': form})


def profile(request, user_id):
    shown_user = Profile.objects.get(pk=user_id)
    return render(request, 'user/profile_view.html', {
        'shown_user': shown_user,
    })


@login_required
def edit(request):
    if request.method == 'POST':
        user_edit_form = EditUserForm(request.POST, instance=request.user)
        profile_edit_form = EditProfileForm(request.POST, instance=request.user.profile)

        if user_edit_form.is_valid() and profile_edit_form.is_valid():
            user = user_edit_form.save()
            user.refresh_from_db()

            profile = profile_edit_form.save()
            profile.refresh_from_db()

            user.save()
            profile.save()

            return redirect('/user/' + str(request.user.pk) + '/?edit_successful')
    else:
        user_edit_form = EditUserForm(instance=request.user)
        profile_edit_form = EditProfileForm(instance=request.user.profile)

    return render(request, 'user/edit.html', {
        'user_edit_form': user_edit_form,
        'profile_edit_form': profile_edit_form
    })
